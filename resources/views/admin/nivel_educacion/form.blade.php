<div class="form-group">
    <label for="nombre" class="col-lg-3 control-label requerido">Nombre</label>

    <div class="col-lg-8">
      <input type="text" class="form-control" name="nombre" id="nombre" value="{{old('nombre', $data->nombre ?? '')}}" required>
    </div>
</div>
<div class="form-group">
  <label for="activo" class="col-lg-3 control-label">¿Es Bachillerato?</label>

  <div class="col-lg-8" style="margin-top: 7px;">
    <select name="es_bachillerato" id="es_bachillerato" class="form-control">
        <option value="1">Sí</option>
        <option value="0">No</option>
    </select>
  </div>
</div>
