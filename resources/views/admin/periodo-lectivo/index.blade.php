@extends("lte.layout")
@section('titulo')
    Periodos Lectivos
@endsection

@section('scripts')
    <script src="{{asset("assets/pages/scripts/admin/index.js")}}" type="text/javascript"></script>
@endsection

@section('contenido')
    <div class="row">
        <div class="col-lg-12">
            @include('includes.form-error')
            @include('includes.mensaje')
            <div class="box box-success">
                <div class="box-header with-border">
                    <h3 class="box-title">Periodos Lectivos</h3>
                    <div class="box-tools pull-right">
                        <a href="{{route('crear_modalidad')}}" class="btn btn-block btn-success btn-sm">
                            <i class="fa fa-fw fa-plus-circle"></i> Nuevo registro
                        </a>
                    </div>
                </div>
                <table class="table table-striped table-bordered table-hover" id="tabla-data">
                    <thead>
                        <tr>
                            <th>Id</th>
                            <th>Año Inicial</th>
                            <th>Año Final</th>
                            <th>Estado</th>
                            <th class="width70"></th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($datas as $data)
                            <tr>
                                <td>{{$data->id_periodo_lectivo}}</td>
                                <td>{{$data->pe_anio_inicio}}</td>
                                <td>{{$data->pe_anio_fin}}</td>
                                <td>{{$data->pe_descripcion}}</td>
                                <td>
                                    <a href="{{route('editar_periodo_lectivo', ['id' => $data->id_periodo_lectivo])}}" class="btn-accion-tabla tooltipsC" title="Editar este Periodo Lectivo">
                                        <i class="fa fa-fw fa-pencil"></i>
                                    </a>
                                    <form action="{{route('cerrar_periodo_lectivo', ['id' => $data->id_periodo_lectivo])}}" class="d-inline form-eliminar" method="POST">
                                        @csrf
                                        @method("delete")
                                        <button type="submit" class="btn-accion-tabla cerrar tooltipsC" title="Cerrar este Periodo Lectivo">
                                            <i class="fa fa-fw fa-window-close text-danger"></i>
                                        </button>
                                    </form>
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
@endsection
