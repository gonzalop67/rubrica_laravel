<header class="main-header">
    <!-- Logo -->
    <a href="/" class="logo">
        <!-- mini logo for sidebar mini 50x50 pixels -->
        <span class="logo-mini"><b>SIAE</b></span>
        <!-- logo for regular state and mobile devices -->
        <span class="logo-lg"><b>SIAE <?php //echo $this->session->userdata("periodo") ?></b></span>
    </a>
    <!-- Header Navbar: style can be found in header.less -->
    <nav class="navbar navbar-static-top">
        <!-- Sidebar toggle button-->
        <a href="#" class="sidebar-toggle" data-toggle="push-menu" role="button">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
        </a>
        <div class="navbar-custom-menu">
            <ul class="nav navbar-nav">
                <!-- Messages: style can be found in dropdown.less-->
                <li class="dropdown messages-menu">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                    <i class="fa fa-comments-o"></i>
                    <span class="label label-warning">12</span>
                    </a>
                    <ul class="dropdown-menu">
                    <li class="header">You have 12 comments</li>
                    <li>
                        <!-- inner menu: contains the actual data -->
                        <ul class="menu">
                        <li><!-- start message -->
                            <a href="#">
                            <div class="pull-left">
                                <img src="{{asset("assets/template/dist/img/student-male-avatar.jpg")}}" class="img-circle" alt="User Image">
                            </div>
                            <h4>
                                QUINTIN OSCAR
                            </h4>
                            <p>Primera vez que me quedo a supletorios...</p>
                            </a>
                        </li>
                        <!-- end message -->
                        <li>
                            <a href="#">
                            <div class="pull-left">
                                <img src="{{asset("assets/template/dist/img/student-female-avatar.jpg")}}" class="img-circle" alt="User Image">
                            </div>
                            <h4>
                                CHANO CRUZ
                            </h4>
                            <p>Me quedé en mate... que iras!!!</p>
                            </a>
                        </li>
                        <li>
                            <a href="#">
                            <div class="pull-left">
                                <img src="{{asset("assets/template/dist/img/user4-128x128.jpg")}}" class="img-circle" alt="User Image">
                            </div>
                            <h4>
                                Developers
                            </h4>
                            <p>Why not buy a new awesome theme?</p>
                            </a>
                        </li>
                        <li>
                            <a href="#">
                            <div class="pull-left">
                                <img src="{{asset("assets/template/dist/img/user3-128x128.jpg")}}" class="img-circle" alt="User Image">
                            </div>
                            <h4>
                                Sales Department
                            </h4>
                            <p>Why not buy a new awesome theme?</p>
                            </a>
                        </li>
                        <li>
                            <a href="#">
                            <div class="pull-left">
                                <img src="{{asset("assets/template/dist/img/user4-128x128.jpg")}}" class="img-circle" alt="User Image">
                            </div>
                            <h4>
                                Reviewers
                            </h4>
                            <p>Why not buy a new awesome theme?</p>
                            </a>
                        </li>
                        </ul>
                    </li>
                    <li class="footer"><a href="#">See All Comments</a></li>
                    </ul>
                </li>
                <!-- User Account: style can be found in dropdown.less -->
                <li class="dropdown user user-menu">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                        <?php $foto_usuario = session()->get('foto_usuario') ?? 'default-avatar.png'; ?>
                        <img src="{{asset("assets/template/dist/img/$foto_usuario")}}" class="user-image" alt="User Image">
                        <span class="hidden-xs">{{session()->get('nombre_usuario') ?? 'Invitado'}}</span>
                        <b class="caret"></b>
                    </a>
                    <ul class="dropdown-menu">

                        <li class="user-header">
                            <img src="{{asset("assets/template/dist/img/$foto_usuario")}}" class="img-circle" alt="User Image">
                            <p style="font-size: 11pt">
                                {{session()->get('nombre_usuario') ?? 'Invitado'}} <br> {{session()->get('perfil_nombre') ?? 'Invitado'}}
                            <small>{{session()->get('periodo_lectivo')}}</small>
                            </p>
                        </li>

                        <!-- Menu Body -->

                        <!-- Menu Footer-->
                        <li class="user-footer">
                            <div class="pull-left">
                            <a href="#" class="btn btn-default btn-flat">Perfil</a>
                            </div>
                            <div class="pull-right">
                                <a href="{{route('logout')}}" class="btn btn-default btn-flat">Salir</a>
                            {{-- <a href="<?php //echo base_url(); ?>auth/logout" class="btn btn-default btn-flat">Salir</a> --}}
                            </div>
                        </li>

                    </ul>

                </li>
            </ul>
        </div>
    </nav>
</header>
