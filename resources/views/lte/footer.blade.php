<footer class="main-footer">
    <div class="pull-right hidden-xs">
      <b>Version</b> 2.4.0
    </div>
    <strong>&copy; {{date("Y")}} <a href="{{session()->get('urlInstitucion')}}">{{session()->get('nombreInstitucion')}}</a>.</strong> Todos los derechos reservados.
</footer>