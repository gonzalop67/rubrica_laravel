<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>SIAE Web | @yield('titulo', 'Dashboard')</title>
    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <!-- Bootstrap 3.3.7 -->
    <link rel="stylesheet" href="{{asset("assets/template/bootstrap/css/bootstrap.min.css")}}">
    <!-- jquery-ui -->
    <link rel="stylesheet" href="{{asset("assets/template/jquery-ui/jquery-ui.css")}}">
    <!-- Alertify -->
    <link rel="stylesheet" href="{{asset("assets/template/alertify/themes/alertify.core.css")}}">
    <link rel="stylesheet" href="{{asset("assets/template/alertify/themes/alertify.default.css")}}">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="{{asset("assets/template/font-awesome/css/font-awesome.min.css")}}">
    <!-- Theme style -->
    <link rel="stylesheet" href="{{asset("assets/template/dist/css/AdminLTE.min.css")}}">
    <!-- AdminLTE Skins. Choose a skin from the css/skins
    folder instead of downloading all of them to reduce the load. -->
    <link rel="stylesheet" href="{{asset("assets/template/dist/css/skins/_all-skins.min.css")}}">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.css">
    @yield('styles')
    <link rel="stylesheet" href="{{asset("assets/css/custom.css")}}">
    <!-- plotly -->
    {{-- <script src="{{asset("assets/js/plotly-latest.min.js")}}"></script> --}}
    <link rel="shortcut icon" href="{{asset("img/favicon.ico")}}">
</head>
<body class="hold-transition skin-blue sidebar-mini">
    <!-- Site wrapper -->
    <div class="wrapper">
        <!-- Inicio Header -->
        @include("lte.header")
        <!-- Fin Header -->
        <!-- Inicio Aside -->
        @include("lte.aside")
        <!-- Fin Aside -->
        <div class="content-wrapper">
            <!-- Content Header (Page header) -->
            <section class="content">
                @yield('contenido')
            </section>
        </div>
        <!-- Inicio Footer -->
        @include("lte.footer")
        <!-- Fin Footer -->
    </div>
    <!-- jQuery 3 -->
    <script src="{{asset("assets/template/jquery/jquery.min.js")}}"></script>
    <!-- Alertify -->
    <script src="{{asset("assets/template/alertify/lib/alertify.min.js")}}"></script>
    <!-- Bootstrap 3.3.7 -->
    <script src="{{asset("assets/template/bootstrap/js/bootstrap.min.js")}}"></script>
    <!-- jquery-ui -->
    <script src="{{asset("assets/template/jquery-ui/jquery-ui.js")}}"></script>
    <!-- SlimScroll -->
    <script src="{{asset("assets/template/jquery-slimscroll/jquery.slimscroll.min.js")}}"></script>
    <!-- FastClick -->
    <script src="{{asset("assets/template/fastclick/lib/fastclick.js")}}"></script>
    <!-- AdminLTE App -->
    <script src="{{asset("assets/template/dist/js/adminlte.min.js")}}"></script>
    <!-- AdminLTE for demo purposes -->
    <script src="{{asset("assets/template/dist/js/demo.js")}}"></script>
    <!-- jQuery Validation -->
    @yield('scriptsPlugins')
    <script src="{{asset("assets/js/jquery-validation/jquery.validate.min.js")}}"></script>
    <script src="{{asset("assets/js/jquery-validation/localization/messages_es.min.js")}}"></script>
    <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.js"></script>
    <script src="{{asset("assets/js/scripts.js")}}"></script>
    <script src="{{asset("assets/js/funciones.js")}}"></script>
    @yield('scripts')
</body>
</html>
