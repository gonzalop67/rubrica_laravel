<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>SIAE Web 2 | Log in</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <!-- Bootstrap 3.3.7 -->
    <link rel="stylesheet" href="{{asset("assets/template/bootstrap/css/bootstrap.min.css")}}">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="{{asset("assets/template/font-awesome/css/font-awesome.min.css")}}">
    <!-- Theme style -->
    <link rel="stylesheet" href="{{asset("assets/template/dist/css/AdminLTE.min.css")}}">
    <link rel="shortcut icon" href="{{asset("img/favicon.ico")}}">
  <style>
    .blanco {
      color: #FFF;
    }
  </style>
</head>
<body class="hold-transition login-page" style="background: url('{{asset("assets/images/loginFont.jpg")}}')">
    <div class="login-box blanco">
        <div class="login-logo">
            <h2>S. I. A. E.</h2>
        </div>
        <!-- /.login-logo -->
        <div class="login-box-body">
            <p class="login-box-msg">Introduzca sus datos de ingreso</p>
            @if ($errors->any())
                <div class="alert alert-danger alert-dismissible">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">x</button>
                    <div class="alert-text">
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                </div>
            @endif
            <form action="{{route('login_post')}}" method="POST" autocomplete="off">
                @csrf
                <div class="form-group has-feedback">
                    <input type="text" class="form-control" value="{{old('login')}}" placeholder="Usuario" id="login" name="login">
                    <span class="form-control-feedback">
                      <img src="{{asset("assets/images/if_user_male_172625.png")}}" height="16px" width="16px">
                    </span>
                </div>
                <div class="form-group has-feedback">
                    <input type="password" class="form-control" placeholder="Password" id="password" name="password">
                    <span class="form-control-feedback">
                      <img src="{{asset("assets/images/if_91_171450.png")}}" height="16px" width="16px">
                    </span>
                </div>
                <div class="form-group has-feedback">
                    <select class="form-control" id="periodo" name="periodo">
                        <option value="">Seleccione el periodo lectivo...</option>
                        @foreach ($periodos_lectivos as $periodo_lectivo)
                            <option value="{{$periodo_lectivo->id_periodo_lectivo}}">
                                {{$periodo_lectivo->pe_anio_inicio . " - " . $periodo_lectivo->pe_anio_fin . " [" . $periodo_lectivo->pe_descripcion ."]"}}
                            </option>
                        @endforeach
                	</select>
                </div>
                <div class="form-group has-feedback">
                    <select class="form-control" id="perfil" name="perfil">
                    	<option value="">Seleccione su perfil...</option>
                        @foreach ($perfiles as $perfil)
                            <option value="{{$perfil->id}}">{{$perfil->nombre}}</option>
                        @endforeach
                    </select>
                </div>
                <div class="row">
                    <div class="col-xs-12">
                        <button type="submit" class="btn btn-danger btn-block btn-flat" id="btnEnviar">Ingresar</button>
                    </div>
                </div>
            </form>
        </div>
        <!-- /.login-box-body -->
    </div>
    <!-- /.login-box -->
    <footer style="margin-top: -20px; text-align: center; color: white;">
		.: &copy; {{date("Y")}} - {{$institucion[0]}} :.
	</footer>
</body>
</html>
