<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'Seguridad\LoginController@index');
Route::get('seguridad/login', 'Seguridad\LoginController@index')->name('login');
Route::post('seguridad/login', 'Seguridad\LoginController@login')->name('login_post');
Route::get('seguridad/logout', 'Seguridad\LoginController@logout')->name('logout');
Route::group(['prefix' => 'admin', 'namespace' => 'Admin', 'middleware' => 'auth'], function () {
    Route::get('', 'AdminController@index');
    /*RUTAS DEL MENU*/
    Route::get('menu', 'MenuController@index')->name('menu');
    Route::get('menu/crear', 'MenuController@crear')->name('crear_menu');
    Route::post('menu', 'MenuController@guardar')->name('guardar_menu');
    Route::get('menu/{id}/editar', 'MenuController@editar')->name('editar_menu');
    Route::put('menu/{id}', 'MenuController@actualizar')->name('actualizar_menu');
    Route::get('menu/{id}/eliminar', 'MenuController@eliminar')->name('eliminar_menu');
    Route::post('menu/guardar-orden', 'MenuController@guardarOrden')->name('guardar_orden');
    /*RUTAS PERFIL*/
    Route::get('perfil', 'PerfilController@index')->name('perfil');
    Route::get('perfil/crear', 'PerfilController@crear')->name('crear_perfil');
    Route::post('perfil', 'PerfilController@guardar')->name('guardar_perfil');
    Route::get('perfil/{id}/editar', 'PerfilController@editar')->name('editar_perfil');
    Route::put('perfil/{id}', 'PerfilController@actualizar')->name('actualizar_perfil');
    Route::delete('perfil/{id}', 'PerfilController@eliminar')->name('eliminar_perfil');
    /*RUTAS MENU_PERFIL*/
    Route::get('menu-perfil', 'MenuPerfilController@index')->name('menu_perfil');
    Route::post('menu-perfil', 'MenuPerfilController@guardar')->name('guardar_menu_perfil');
    /*RUTAS MODALIDAD*/
    Route::get('modalidad', 'ModalidadController@index')->name('modalidad');
    Route::get('modalidad/crear', 'ModalidadController@crear')->name('crear_modalidad');
    Route::post('modalidad', 'ModalidadController@guardar')->name('guardar_modalidad');
    Route::get('modalidad/{id}/editar', 'ModalidadController@editar')->name('editar_modalidad');
    Route::put('modalidad/{id}', 'ModalidadController@actualizar')->name('actualizar_modalidad');
    Route::delete('modalidad/{id}', 'ModalidadController@eliminar')->name('eliminar_modalidad');
    /*RUTAS PERIODO_LECTIVO*/
    Route::get('periodo_lectivo', 'PeriodoLectivoController@index')->name('periodo_lectivo');
    Route::get('periodo_lectivo/{id}/editar', 'PeriodoLectivoController@editar')->name('editar_periodo_lectivo');
    Route::post('periodo_lectivo/{id}/cerrar', 'PeriodoLectivoController@cerrar')->name('cerrar_periodo_lectivo');
    /*RUTAS NIVEL EDUCACION*/
    Route::get('nivel_educacion', 'NivelEducacionController@index')->name('nivel_educacion');
    Route::get('nivel_educacion/crear', 'NivelEducacionController@crear')->name('crear_nivel_educacion');
    Route::post('nivel_educacion', 'NivelEducacionController@guardar')->name('guardar_nivel_educacion');
    Route::get('nivel_educacion/{id}/editar', 'NivelEducacionController@editar')->name('editar_nivel_educacion');
    Route::put('nivel_educacion/{id}', 'NivelEducacionController@actualizar')->name('actualizar_nivel_educacion');
    Route::delete('nivel_educacion/{id}', 'NivelEducacionController@eliminar')->name('eliminar_nivel_educacion');
});

//Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
