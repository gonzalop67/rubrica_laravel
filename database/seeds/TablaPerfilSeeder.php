<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class TablaPerfilSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $perfiles = [
            'Administrador',
            'Autoridad',
            'DECE',
            'Docente',
            'Inspección',
            'Secretaría',
            'Tutor'
        ];
        foreach ($perfiles as $key => $value) {
            DB::table('sw_perfil')->insert([
                'nombre' => $value
            ]);
        }
    }
}
