<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class UsuarioAdministradorSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('sw_usuario')->insert([
            'login' => 'Administrador',
            'password' => bcrypt('gP67M24e$'),
            'titulo' => 'Ing.',
            'apellidos' => 'Peñaherrera Escobar',
            'nombres' => 'Gonzalo Nicolás',
            'shortname' => 'Ing. Gonzalo Peñaherrera',
            'fullname' => 'Peñaherrera Escobar Gonzalo Nicolás',
            'foto' => 'gonzalofoto.jpg',
            'genero' => 'M',
            'activo' => 1
        ]);

        DB::table('sw_usuario_perfil')->insert([
            'perfil_id' => 1,
            'usuario_id' => 1,
            'estado' => 1
        ]);
    }
}
