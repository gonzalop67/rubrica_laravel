<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class PrimerPeriodoLectivoSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('sw_periodo_lectivo')->insert([
            'id_periodo_estado' => '1',
            'id_institucion' => '1',
            'pe_anio_inicio' => date("Y"),
            'pe_anio_fin' => date("Y") + 1
        ]);
    }
}
