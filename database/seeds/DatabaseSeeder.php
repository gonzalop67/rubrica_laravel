<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->truncateTablas([
            'sw_institucion',
            'sw_menu',
            'sw_modalidad',
            'sw_perfil',
            'sw_periodo_estado',
            'sw_periodo_lectivo',
            'sw_usuario',
            'sw_menu_perfil',
            'sw_usuario_perfil'
        ]);
        $this->call(TablaPerfilSeeder::class);
        $this->call(TablaMenuSeeder::class);
        $this->call(TablaModalidadSeeder::class);
        $this->call(TablaInstitucionSeeder::class);
        $this->call(TablaPeriodoEstadoSeeder::class);
        $this->call(PrimerPeriodoLectivoSeeder::class);
        $this->call(UsuarioAdministradorSeeder::class);
    }

    protected function truncateTablas(array $tablas)
    {
        DB::statement('SET FOREIGN_KEY_CHECKS = 0;');
        foreach ($tablas as $tabla) {
            DB::table($tabla)->truncate();
        }
        DB::statement('SET FOREIGN_KEY_CHECKS = 1;');
    }
}
