<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class TablaPeriodoEstadoSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $estados = [
            'ACTUAL',
            'TERMINADO',
        ];
        foreach ($estados as $key => $value) {
            DB::table('sw_periodo_estado')->insert([
                'pe_descripcion' => $value
            ]);
        }
    }
}
