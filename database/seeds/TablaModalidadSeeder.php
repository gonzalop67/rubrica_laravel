<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class TablaModalidadSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('sw_modalidad')->insert([
            'nombre' => 'PRESENCIAL',
            'activo' => '1',
            'orden' => 1
        ]);
    }
}
