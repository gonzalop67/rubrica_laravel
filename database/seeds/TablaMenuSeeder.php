<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class TablaMenuSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('sw_menu')->insert([
            'nombre' => 'Administración',
            'url' => '#',
            'icono' => 'fa-gear',
            'orden' => 1
        ]);

        DB::table('sw_menu_perfil')->insert([
            'perfil_id' => 1,
            'menu_id' => 1
        ]);

        DB::table('sw_menu')->insert([
            'nombre' => 'Modalidades',
            'url' => 'admin/modalidad',
            'icono' => 'fa-mortar-board',
            'menu_id' => 1,
            'orden' => 1
        ]);

        DB::table('sw_menu_perfil')->insert([
            'perfil_id' => 1,
            'menu_id' => 2
        ]);

        DB::table('sw_menu')->insert([
            'nombre' => 'Períodos Lectivos',
            'url' => 'admin/periodo_lectivo',
            'icono' => 'fa-calendar',
            'menu_id' => 1,
            'orden' => 2
        ]);

        DB::table('sw_menu_perfil')->insert([
            'perfil_id' => 1,
            'menu_id' => 3
        ]);

        DB::table('sw_menu')->insert([
            'nombre' => 'Perfiles',
            'url' => 'admin/perfil',
            'icono' => 'fa-user-o',
            'menu_id' => 1,
            'orden' => 3
        ]);

        DB::table('sw_menu_perfil')->insert([
            'perfil_id' => 1,
            'menu_id' => 4
        ]);

        DB::table('sw_menu')->insert([
            'nombre' => 'Menús',
            'url' => 'admin/menu',
            'icono' => 'fa-bars',
            'menu_id' => 1,
            'orden' => 4
        ]);

        DB::table('sw_menu_perfil')->insert([
            'perfil_id' => 1,
            'menu_id' => 5
        ]);

        DB::table('sw_menu')->insert([
            'nombre' => 'Menús - Perfiles',
            'url' => 'admin/menu-perfil',
            'icono' => 'fa-cogs',
            'menu_id' => 1,
            'orden' => 5
        ]);

        DB::table('sw_menu_perfil')->insert([
            'perfil_id' => 1,
            'menu_id' => 6
        ]);

        DB::table('sw_menu')->insert([
            'nombre' => 'Definiciones',
            'url' => '#',
            'icono' => 'fa-gear',
            'orden' => 2
        ]);

        DB::table('sw_menu_perfil')->insert([
            'perfil_id' => 1,
            'menu_id' => 7
        ]);

        DB::table('sw_menu')->insert([
            'nombre' => 'Niveles de Educación',
            'url' => 'admin/nivel_educacion',
            'icono' => 'fa-mortar-board',
            'menu_id' => 7,
            'orden' => 1
        ]);

        DB::table('sw_menu_perfil')->insert([
            'perfil_id' => 1,
            'menu_id' => 8
        ]);
    }
}
