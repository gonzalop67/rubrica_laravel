<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CrearTablaPeriodoLectivo extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sw_periodo_lectivo', function (Blueprint $table) {
            $table->id('id_periodo_lectivo');
            $table->unsignedBigInteger('id_periodo_estado');
            $table->foreign('id_periodo_estado', 'fk_periodolectivo_estado')->references('id_periodo_estado')->on('sw_periodo_estado')->onDelete('restrict')->onUpdate('restrict');
            $table->unsignedBigInteger('id_institucion');
            $table->foreign('id_institucion', 'fk_periodolectivo_institucion')->references('id_institucion')->on('sw_institucion')->onDelete('restrict')->onUpdate('restrict');
            $table->unsignedInteger('pe_anio_inicio');
            $table->unsignedInteger('pe_anio_fin');
            $table->date('pe_fecha_inicio')->default(date('Y-m-d'));
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sw_periodo_lectivo');
    }
}
