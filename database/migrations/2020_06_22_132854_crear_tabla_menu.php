<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CrearTablaMenu extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sw_menu', function (Blueprint $table) {
            $table->id();
            $table->unsignedInteger('menu_id')->default(0);
            $table->string('nombre', 32);
            $table->string('url', 64);
            $table->unsignedInteger('orden')->default(0);
            $table->string('icono', 25)->nullable();
            $table->charset = 'utf8mb4';
            $table->collation = 'utf8mb4_spanish_ci';
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sw_menu');
    }
}
