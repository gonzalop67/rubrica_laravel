<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CrearTablaInstitucion extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sw_institucion', function (Blueprint $table) {
            $table->id('id_institucion');
            $table->string('in_nombre', 64);
            $table->string('in_direccion', 45);
            $table->string('in_telefono', 45);
            $table->string('in_nom_rector', 45);
            $table->string('in_nom_vicerrector', 45);
            $table->string('in_nom_secretario', 45);
            $table->string('in_url', 64);
            $table->string('in_logo', 64);
            $table->string('in_amie', 16);
            $table->string('in_ciudad', 32);
            $table->boolean('in_copiar_y_pegar')->default(true);
            $table->charset = 'utf8mb4';
            $table->collation = 'utf8mb4_spanish_ci';
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sw_institucion');
    }
}
