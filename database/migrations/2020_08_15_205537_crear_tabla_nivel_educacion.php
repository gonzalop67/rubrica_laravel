<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CrearTablaNivelEducacion extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sw_nivel_educacion', function (Blueprint $table) {
            $table->id();
            $table->string('nombre', 48);
            $table->unsignedInteger('es_bachillerato')->default(0);
            $table->unsignedInteger('orden')->default(0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sw_nivel_educacion');
    }
}
