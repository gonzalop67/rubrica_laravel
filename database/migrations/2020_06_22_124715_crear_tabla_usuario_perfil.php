<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CrearTablaUsuarioPerfil extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sw_usuario_perfil', function (Blueprint $table) {
            $table->unsignedBigInteger('usuario_id');
            $table->foreign('usuario_id', 'fk_usuarioperfil_usuario')->references('id')->on('sw_usuario')->onDelete('restrict')->onUpdate('restrict');
            $table->unsignedBigInteger('perfil_id');
            $table->foreign('perfil_id', 'fk_usuarioperfil_perfil')->references('id')->on('sw_perfil')->onDelete('restrict')->onUpdate('restrict');
            $table->tinyInteger('estado', 1);
            $table->charset = 'utf8mb4';
            $table->collation = 'utf8mb4_spanish_ci';
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sw_usuario_perfil');
    }
}
