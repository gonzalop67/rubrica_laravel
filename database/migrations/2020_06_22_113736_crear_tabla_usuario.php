<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CrearTablaUsuario extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sw_usuario', function (Blueprint $table) {
            $table->id();
            $table->string('titulo', 5);
            $table->string('apellidos', 32);
            $table->string('nombres', 32);
            $table->string('shortname', 45);
            $table->string('fullname', 64);
            $table->string('login', 24);
            $table->string('password', 100);
            $table->string('foto', 100);
            $table->string('genero', 1);
            $table->tinyInteger('activo');
            $table->charset = 'utf8mb4';
            $table->collation = 'utf8mb4_spanish_ci';
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sw_usuario');
    }
}
