<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CrearTablaPerfil extends Migration
{
    public function up()
    {
        Schema::create('sw_perfil', function (Blueprint $table) {
            $table->id();
            $table->string('nombre', 32)->unique();
            $table->charset = 'utf8mb4';
            $table->collation = 'utf8mb4_spanish_ci';
        });
    }

    public function down()
    {
        Schema::dropIfExists('sw_perfil');
    }
}
