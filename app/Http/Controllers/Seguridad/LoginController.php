<?php

namespace App\Http\Controllers\Seguridad;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Admin\Institucion;
use App\Models\Admin\Perfil;
use App\Models\Admin\PeriodoLectivo;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Support\Facades\DB;

class LoginController extends Controller
{
    use AuthenticatesUsers;
    protected $redirectTo = '/admin';

    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }

    public function index()
    {
        $periodos_lectivos = DB::table('sw_periodo_lectivo')
        ->join('sw_periodo_estado',
                'sw_periodo_lectivo.id_periodo_estado',
                '=',
                'sw_periodo_estado.id_periodo_estado')
        ->select('sw_periodo_lectivo.*', 'sw_periodo_estado.pe_descripcion')
        ->get();
        $perfiles = Perfil::orderBy('nombre')->get();
        $institucion = Institucion::firstOrFail()->pluck('in_nombre')->toArray();
        return view('seguridad.index', compact('perfiles', 'periodos_lectivos', 'institucion'));
    }

    protected function authenticated(Request $request, $user)
    {
        $perfiles = $user->perfiles()
            ->where('id', $request->perfil)
            ->where('estado', 1)
            ->get();
        $periodo_lectivo = PeriodoLectivo::where('id_periodo_lectivo', $request->periodo)->get()->toArray();
        $institucion = Institucion::where('id_institucion', 1)->get()->toArray();
        if ($perfiles->isNotEmpty()) {
            $user->setSession($perfiles->toArray(), $periodo_lectivo, $institucion);
        } else {
            $this->guard()->logout();
            $request->session()->invalidate();
            return redirect('seguridad/login')->withErrors(['error' => 'El usuario no tiene activo el perfil seleccionado']);
        }
    }

    public function username()
    {
        return 'login';
    }
}
