<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\ValidacionNivelEducacion;
use App\Models\Admin\NivelEducacion;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class NivelEducacionController extends Controller
{
    public function index()
    {
        $data = NivelEducacion::orderBy('orden')->get();
        return view('admin.nivel_educacion.index', compact('data'));
    }

    public function crear()
    {
        return view('admin.nivel_educacion.crear');
    }

    public function guardar(ValidacionNivelEducacion $request)
    {
        $nivel_educacion = new NivelEducacion();
        $nivel_educacion->nombre = $request->nombre;
        $nivel_educacion->es_bachillerato = $request->es_bachillerato;
        $orden = DB::select("SELECT MAX(orden) AS secuencial FROM sw_nivel_educacion");
        if ($orden[0]->secuencial == null) {
            $secuencial = 1;
        } else {
            $secuencial = $orden[0]->secuencial + 1;
        }
        $nivel_educacion->orden = $secuencial;
        $nivel_educacion->save();
        return redirect('admin/nivel_educacion')->with('mensaje', 'Nivel de Educación creado con éxito');
    }

    public function editar($id)
    {
        $data = NivelEducacion::findOrFail($id);
        return view('admin.nivel_educacion.editar', compact('data'));
    }

    public function actualizar(ValidacionNivelEducacion $request, $id)
    {
        NivelEducacion::findOrFail($id)->update($request->all());
        return redirect('admin/nivel_educacion')->with('mensaje', 'Nivel de Educación actualizado con exito');
    }

    public function eliminar(Request $request, $id)
    {
        if ($request->ajax()) {
            if (NivelEducacion::destroy($id)) {
                return response()->json(['mensaje' => 'ok']);
            } else {
                return response()->json(['mensaje' => 'ng']);
            }
        } else {
            abort(404);
        }
    }
}
