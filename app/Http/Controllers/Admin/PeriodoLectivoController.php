<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Admin\PeriodoLectivo;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class PeriodoLectivoController extends Controller
{
    public function index()
    {
        $datas = DB::table('sw_periodo_lectivo')
                 ->join('sw_periodo_estado', 
                        'sw_periodo_estado.id_periodo_estado', '=', 'sw_periodo_lectivo.id_periodo_estado')
                 ->select('id_periodo_lectivo', 'pe_anio_inicio', 'pe_anio_fin', 'pe_descripcion')
                 ->get();
        return view('admin.periodo-lectivo.index', compact('datas'));
    }

    public function create()
    {
        //
    }

    public function store(Request $request)
    {
        //
    }

    public function show($id)
    {
        //
    }

    public function edit($id)
    {
        //
    }

    public function update(Request $request, $id)
    {
        //
    }

    public function destroy($id)
    {
        //
    }
}
