<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\ValidacionPerfil;
use App\Models\Admin\Perfil;
use Illuminate\Http\Request;

class PerfilController extends Controller
{
    public function index()
    {
        $datas = Perfil::orderBy('nombre')->get();
        return view('admin.perfil.index', compact('datas'));
    }

    public function crear()
    {
        return view('admin.perfil.crear');
    }

    public function guardar(ValidacionPerfil $request)
    {
        Perfil::create($request->all());
        return redirect('admin/perfil')->with('mensaje', 'Perfil creado con éxito');
    }

    public function editar($id)
    {
        $data = Perfil::findOrFail($id);
        return view('admin.perfil.editar', compact('data'));
    }

    public function actualizar(ValidacionPerfil $request, $id)
    {
        Perfil::findOrFail($id)->update($request->all());
        return redirect('admin/perfil')->with('mensaje', 'Perfil actualizado con éxito');
    }

    public function eliminar(Request $request, $id)
    {
        if ($request->ajax()) {
            if (Perfil::destroy($id)) {
                return response()->json(['mensaje' => 'ok']);
            } else {
                return response()->json(['mensaje' => 'ng']);
            }
        } else {
            abort(404);
        }
    }
}
