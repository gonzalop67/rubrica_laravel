<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\ValidacionModalidad;
use App\Models\Admin\Modalidad;
use Illuminate\Http\Request;

class ModalidadController extends Controller
{
    public function index()
    {
        $data = Modalidad::orderBy('nombre')->get();
        return view('admin.modalidad.index', compact('data'));
    }

    public function crear()
    {
        return view('admin.modalidad.crear');
    }

    public function guardar(ValidacionModalidad $request)
    {
        // dd($request->all());
        $modalidad = new Modalidad();
        $modalidad->nombre = $request->nombre;
        $modalidad->activo = $request->activo == 'on' ? 1 : 0;
        $modalidad->save();
        return redirect('admin/modalidad')->with('mensaje', 'Modalidad creada con éxito');
    }

    public function editar($id)
    {
        $data = Modalidad::findOrFail($id);
        return view('admin.modalidad.editar', compact('data'));
    }

    public function actualizar(ValidacionModalidad $request, $id)
    {
        // Modalidad::findOrFail($id)->update($request->all());
        $modalidad = Modalidad::findOrFail($id);
        $modalidad->nombre = $request->nombre;
        $modalidad->activo = $request->activo == 'on' ? 1 : 0;
        $modalidad->save();
        return redirect('admin/modalidad')->with('mensaje', 'Modalidad actualizada con exito');
    }

    public function eliminar(Request $request, $id)
    {
        if ($request->ajax()) {
            if (Modalidad::destroy($id)) {
                return response()->json(['mensaje' => 'ok']);
            } else {
                return response()->json(['mensaje' => 'ng']);
            }
        } else {
            abort(404);
        }
    }
}
