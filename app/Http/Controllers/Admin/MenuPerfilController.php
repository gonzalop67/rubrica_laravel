<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Admin\Menu;
use App\Models\Admin\Perfil;
use Illuminate\Http\Request;

class MenuPerfilController extends Controller
{
    public function index()
    {
        $perfiles = Perfil::orderBy('nombre')->pluck('nombre', 'id')->toArray();
        $menus = Menu::getMenu();
        $menusPerfiles = Menu::with('perfiles')->get()->pluck('perfiles', 'id')->toArray();
        return view('admin.menu-perfil.index', compact('perfiles', 'menus', 'menusPerfiles'));
    }

    public function guardar(Request $request)
    {
        if ($request->ajax()) {
            $menus = new Menu();
            if ($request->input('estado') == 1) {
                $menus->find($request->input('menu_id'))->perfiles()->attach($request->input('perfil_id'));
                return response()->json(['respuesta' => 'El perfil se asignó correctamente']);
            } else {
                $menus->find($request->input('menu_id'))->perfiles()->detach($request->input('perfil_id'));
                return response()->json(['respuesta' => 'El rol se eliminó correctamente']);
            }
        } else {
            abort(404);
        }
    }
}
