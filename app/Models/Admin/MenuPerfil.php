<?php

namespace App\Models\Admin;

use Illuminate\Database\Eloquent\Model;

class MenuPerfil extends Model
{
    protected $table = "sw_menu_perfil";
}
