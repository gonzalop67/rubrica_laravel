<?php

namespace App\Models\Admin;

use Illuminate\Database\Eloquent\Model;

class PeriodoEstado extends Model
{
    protected $table = "sw_periodo_estado";
    protected $fillable = ['pe_descripcion'];
}
