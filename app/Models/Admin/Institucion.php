<?php

namespace App\Models\Admin;

use Illuminate\Database\Eloquent\Model;

class Institucion extends Model
{
    protected $table = 'sw_institucion';
    protected $guarded = ['id_institucion'];
}
