<?php

namespace App\Models\Admin;

use Illuminate\Database\Eloquent\Model;

class Perfil extends Model
{
    protected $table = "sw_perfil";
    protected $guarded = ['id'];
    public $timestamps = false;
}
