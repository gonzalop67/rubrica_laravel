<?php

namespace App\Models\Admin;

use Illuminate\Database\Eloquent\Model;

class PeriodoLectivo extends Model
{
    protected $table = "sw_periodo_lectivo";
    protected $fillable = ['id_periodo_estado, id_institucion, pe_anio_inicio, pe_anio_fin, pe_fecha_inicio'];

    public function periodo_estados()
    {
        return $this->hasMany(PeriodoEstado::class, 'id_periodo_estado');
    }
}
