<?php

namespace App\Models\Admin;

use Illuminate\Database\Eloquent\Model;

class NivelEducacion extends Model
{
    protected $table = "sw_nivel_educacion";
    protected $guarded = ['id'];
    public $timestamps = false;
}
