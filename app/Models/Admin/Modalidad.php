<?php

namespace App\Models\Admin;

use Illuminate\Database\Eloquent\Model;

class Modalidad extends Model
{
    protected $table = "sw_modalidad";
    protected $guarded = ['id'];
    public $timestamps = false;
}
