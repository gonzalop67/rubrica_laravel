<?php

namespace App\Models\Seguridad;

use App\Models\Admin\Perfil;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Support\Facades\Session;

class Usuario extends Authenticatable
{
    protected $remember_token = false;
    protected $table = "sw_usuario";
    protected $guarded = ['id'];
    public $timestamps = false;

    public function perfiles()
    {
        return $this->belongsToMany(Perfil::class, 'sw_usuario_perfil');
    }

    public function setSession($perfiles, $periodo_lectivo, $institucion)
    {
        Session::put([
            'perfil_id' => $perfiles[0]['id'],
            'perfil_nombre' => $perfiles[0]['nombre'],
            'login' => $this->login,
            'usuario_id' => $this->id,
            'nombre_usuario' => $this->shortname,
            'foto_usuario' => $this->foto,
            'periodo_lectivo_id' => $periodo_lectivo[0]['id_periodo_lectivo'],
            'periodo_lectivo' => $periodo_lectivo[0]['pe_anio_inicio'] . " - " . $periodo_lectivo[0]['pe_anio_fin'],
            'nombreInstitucion' => $institucion[0]['in_nombre'],
            'urlInstitucion' => $institucion[0]['in_url']
        ]);
    }
}
